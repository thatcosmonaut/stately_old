﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Stately;

public class Cube : MonoBehaviour {

	new Rigidbody rigidbody;

	State rootState = new State();

	State idleState = new State();
	State jumpingState = new State();

	void Awake() {
		rigidbody = GetComponent<Rigidbody>();

		InitializeStateMachine();
	}

	void InitializeStateMachine() {
		rootState.StartAt(idleState);
		rootState.Enter();

		idleState.ChangeTo(jumpingState).If(delegate { return Input.GetButtonDown("Jump"); });

		jumpingState.OnEnter = delegate {
			rigidbody.AddForce(300f * Vector3.up);
		};

		jumpingState.OnUpdate = delegate { };
		jumpingState.OnExit = delegate { };

		jumpingState.ChangeTo(idleState).IfSignalCaught("groundCollision");

	}
	
	// Update is called once per frame
	void Update () {
		rootState.Update();
	}

	void OnCollisionEnter(Collision col) {
		if (col.gameObject.name == "Floor") {
			rootState.SendSignal("groundCollision");
		}
	}
}
