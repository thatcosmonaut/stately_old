using UnityEngine;
using System.Collections.Generic;
using System;

namespace Stately
{
    public class State
    {
        public string Name { get; protected set; }

        private State currentState;
        private State startState;
        public Action OnUpdate = delegate { };
        public Action OnFixedUpdate = delegate { };
        public Action OnEnter = delegate { };
        public Action OnExit = delegate { };
        private List<Transition> transitions = new List<Transition>();
        private List<string> signals = new List<string>();

        private float timeElapsed = float.PositiveInfinity;
        private float timerDuration = 0f;
        public bool isTimerExpired = false;
        private bool timerUsesRealTime = false;
        private float previousRealTime;

        private bool isComplete;
        public bool IsComplete { get { return isComplete; } set { isComplete = value; } }

        protected bool isRoot;

        public State(string name = "", bool root = false)
        {
            Name = name;
            isRoot = root;
        }

        public State(State state, string name = "")
        {
            currentState = state;
            Name = name;
        }

        public void Update()
        {
            if (timerUsesRealTime)
            {
                timeElapsed += (Time.realtimeSinceStartup - previousRealTime);
                previousRealTime = Time.realtimeSinceStartup;
            }
            else
            {
                timeElapsed += Time.deltaTime;
            }
            if (timerDuration == 0f)
            {
                isTimerExpired = false;
            }
            else if (timeElapsed >= timerDuration)
            {
                isTimerExpired = true;
            }

            OnUpdate();
            if (currentState != null)
            {
                currentState.Update();
                Transition transition = currentState.GetTransition();
                if (transition != null)
                {
                    State next = transition.GetTarget();
                    transition.OnTransition();
                    Enter(next);
                }
                currentState.ClearSignals();
            }
            if (isRoot)
            {
                ClearSignals();
            }
        }

        public void FixedUpdate()
        {
            OnFixedUpdate();
            if (currentState != null)
            {
                currentState.FixedUpdate();
            }
        }

        public void Enter(State state)
        {
            if (currentState != null)
            {
                currentState.Exit();
            }
            state.Enter();
            currentState = state;
        }

        public void AddTransition(State state, Func<bool> condition)
        {
            Transition transition = new Transition(state, condition);
            transitions.Add(transition);
        }

        public Transition ChangeTo(State state)
        {
            Transition transition = new Transition(state);
            transitions.Add(transition);
            return transition;
        }

        public Transition GetTransition()
        {
            for (var i = 0; i < transitions.Count; i++)
            {
                if (transitions[i].Evaluate(signals))
                {
                    return transitions[i];
                }
            }
            return null;
        }

        public void Enter()
        {
            isComplete = false;
            currentState = startState;
            if (currentState != null)
            {
                currentState.Enter();
            }
            previousRealTime = Time.realtimeSinceStartup;
            OnEnter();
        }

        public void Exit()
        {
            isComplete = false;
            isTimerExpired = false;
            if (currentState != null)
            {
                currentState.Exit();
            }
            OnExit();
        }

        public void ClearOnTransitionTo(State state)
        {
            foreach (Transition transition in transitions)
            {
                if (transition.GetTarget() == state)
                {
                    transition.OnTransition = delegate { };
                }
            }
        }

        public void OnTransitionTo(State state, Action action)
        {
            foreach (Transition transition in transitions)
            {
                if (transition.GetTarget() == state)
                {
                    transition.OnTransition += action;
                }
            }
        }

        public void StartAt(State state)
        {
            startState = state;
        }

        public void SetTimer(float duration, bool useRealTime = false)
        {
            timeElapsed = 0f;
            timerDuration = duration;
            isTimerExpired = false;
            timerUsesRealTime = useRealTime;
        }

        public float GetTimer()
        {
            return timeElapsed;
        }

        public float TimerPercentageComplete()
        {
            if (timerDuration == 0f)
            {
                return 0f;
            }
            return Mathf.Clamp(timeElapsed / timerDuration, 0f, 1f);
        }

        public State CurrentState()
        {
            return currentState;
        }

        public void SendSignal(string signal)
        {
            signals.Add(signal);
            if (currentState != null)
            {
                currentState.SendSignal(signal);
            }
        }

        public void ClearSignals()
        {
            signals.Clear();
            if (currentState != null)
            {
                currentState.ClearSignals();
            }
        }
    }
}
