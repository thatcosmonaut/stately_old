using System;
using System.Collections.Generic;

namespace Stately
{
    public class Condition
    {
        public Condition(Func<bool> test)
        {
            tests = test;
        }

        public Func<bool> tests = delegate { return false; };

        public Condition And(Func<bool> test)
        {
            tests += test;
            return this;
        }
    }

    public class SignalCondition
    {
        string signal = null;

        public SignalCondition(string _signal)
        {
            signal = _signal;
        }

        public bool Check(List<string> signals)
        {
            if (signals.Contains(signal))
            {
                signals.Remove(signal);
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
