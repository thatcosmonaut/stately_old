using System.Collections.Generic;
using System;
using UnityEngine;

namespace Stately
{
    public class Transition
    {
        public Action OnTransition = delegate { };

        private List<Condition> conditions = new List<Condition>();
        private List<SignalCondition> signalConditions = new List<SignalCondition>();
        private List<Delegate[]> invocationLists = new List<Delegate[]>();
        private State target;
#pragma warning disable 0414
        private Func<bool> condition = delegate { return false; };
#pragma warning restore 0414

        public State Target
        {
            get
            {
                return target;
            }
            set { }
        }

        public Transition(State state, Func<bool> test)
        {
            target = state;
            Condition c = new Condition(test);
            conditions.Add(c);
            invocationLists.Add(c.tests.GetInvocationList());
        }

        public Transition(State state)
        {
            target = state;
        }

        public Condition If(Func<bool> test)
        {
            Condition c = new Condition(test);
            conditions.Add(c);
            invocationLists.Add(c.tests.GetInvocationList());
            return c;
        }

        public void IfSignalCaught(string signal)
        {
            SignalCondition c = new SignalCondition(signal);
            signalConditions.Add(c);
        }

        public bool Evaluate(List<String> signals)
        {
            for (int i = 0; i < signalConditions.Count; i++)
            {
                SignalCondition c = signalConditions[i];
                if (c.Check(signals))
                {
                    return true;
                }
            }

            // ORS
            for (int i = 0; i < conditions.Count; i++)
            {
                // ANDS
                //Debug.Log("testing");
                bool valid = true;
                foreach (Func<bool> f in invocationLists[i])
                {
                    if (f() != true)
                    {
                        valid = false;
                        break;
                    }
                    //Debug.Log("successful test");
                }
                if (valid)
                {
                    return true;
                }
            }
            return false;
        }

        public State GetTarget()
        {
            return target;
        }
    }
}
